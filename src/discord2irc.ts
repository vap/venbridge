import { createHash } from "crypto"
import { discordClient } from "./discord"
import { ircClient } from './irc'
import { FormatCode } from './irc/formatting'

const userHashCache = new Map<string, bigint>()
function getUserColor(key: string) {
    let userHash = userHashCache.get(key)
    if (userHash == null) {
        userHash = BigInt("0x" + createHash("sha256").update(key).digest("hex"))
        userHashCache.set(key, userHash)
    }
    return `${(userHash % 14n) + 2n}`
}

discordClient.on("messageCreate", (message) => {
    if (message.channelID !== process.env.DISCORD_CHANNEL) return
    if (message.author.bot) return

    const ircChannel = ircClient.channels.get(process.env.IRC_CHANNEL)
    if (!ircChannel) return

    const color = getUserColor(message.author.id)
    const escapedUser = message.author.username[0] + "\u200B" + message.author.username.slice(1)
    const prefix = `${FormatCode.COLOR}${color}<${escapedUser}>${FormatCode.RESET} `
    let content = message.content

    for (const [, attachment] of message.attachments) content += `\n${attachment.url}`
    for (const embed of message.embeds) {
        content += `\nembed: `
        if (embed.title) content += `${embed.title} - `
        if (embed.description) content += `${embed.description}`
        if (embed.url) content += ` (${embed.url})`
    }

    // TODO: word wrapping with retained formatting
    content = content.replace(/[^\n]{350}/g, "$&\n")
    content = content.replace(/\n+/g, `\n`)

    for (const line of content.split("\n")) {
        ircChannel.say(prefix + line)
    }
})
