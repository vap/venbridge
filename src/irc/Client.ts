import supportsAnsi from "supports-ansi"
import { EventEmitter } from "eventemitter3"
import { Numeric } from './types/Numerics'
import { Message, parseMessage } from './types/Message'
import { ClientSocket } from './types/ClientSocket'
import { Channel } from './types/Channel'
import { FormatCode, codesToAnsi, stripCodes } from './formatting'
import { FlowResult, ReadonlyFlow, createFlow } from './flow'

export type AnyAuth = {
    type: "SASL:PLAIN",
    password: string,
}
export type Auth<T extends AnyAuth["type"]> = Extract<AnyAuth, { type: T }>

export interface ClientOptions {
    host: string
    port: number
    debug?: boolean

    nickname: string
    username: string
    realname: string
    auth: AnyAuth
}

export class Client extends EventEmitter<{
    error: [error: unknown]
    connect: []
    close: []
    message: [message: Message]
    ready: []
}> {
    socket: ClientSocket

    serverSource?: string
    serverCapabilities = new Map<string, string>()
    connCapabilities = new Set<string>()

    isLoggedIn = false

    channels = new Map<string, Channel>()

    constructor(public options: ClientOptions) {
        super()

        // Socket initialization
        this.socket = new ClientSocket(options.host, options.port)
        this.socket.on("read", (data) => this.onData(data))
        this.socket.on("write", (data) => this.debug(`[CLIENT] ${data}`))
        this.socket.on("error", (error) => this.emit("error", error))
        this.socket.on("close", () => this.emit("close"))
    }

    get nickname() { return this.options.nickname }
    get username() { return this.options.username }
    get realname() { return this.options.realname }

    private debug(message: string) {
        if (!this.options.debug) return
        const out = supportsAnsi
            ? codesToAnsi(FormatCode.RESET + message + FormatCode.RESET)
            : stripCodes(message)
        console.log(out)
    }
    private onData(data: string) {
        this.debug(`[SERVER] ${data}`)

        try {
            var message = parseMessage(data)
        } catch (error) {
            return this.emit("error", error)
        }

        this.onMessage(message)
        this.emit("message", message)
    }

    async connect() {
        try {
            await this.socket.connect()
            this.debug("[CONNECTED]")
            await this.init()
        } catch (error) {
            this.emit("error", error)
        }
    }
    close() {
        this.socket.close()
    }
    destroy() {
        this.socket.destroy()
        super.removeAllListeners()
    }

    send(command: string, ...params: string[]) {
        return this.socket.send(command, ...params)
    }

    waitForMatch<T>(exec: (message: Message) => T | null, timeoutMs = 30000) {
        return new Promise<T | null>((resolve) => {
            const timeout = setTimeout(() => {
                this.off("message", listener)
                resolve(null)
            }, timeoutMs)

            const listener = (message: Message) => {
                const match = exec(message)
                if (match == null) return

                clearTimeout(timeout)
                this.off("message", listener)
                resolve(match)
            }
            this.on("message", listener)
        })
    }
    waitForFilter(filter: (message: Message) => boolean, timeoutMs = 30000) {
        return this.waitForMatch(message => filter(message) ? message : null, timeoutMs)
    }
    waitFor(command: string, options?: { sourcename?: string }) {
        return this.waitForFilter(m => m.command === command && (!options?.sourcename || m.source.name === options.sourcename))
    }
    waitForAny(commands: string[], options?: { sourcename?: string }) {
        return this.waitForFilter(m => commands.includes(m.command) && (!options?.sourcename || m.source.name === options.sourcename))
    }
    waitForFlow<const F extends ReadonlyFlow>(flow: F, opts: {
        filter?: (m: Message) => boolean,
        timeoutMs?: number,
    } = {}): Promise<FlowResult<F> | null> {
        const [messages, exec] = createFlow(flow)

        return this.waitForMatch(message => {
            if (opts.filter && !opts.filter(message)) return null

            messages.push(message)
            return exec()
        })
    }

    // Capability negotiation, authentication, etc.
    private async init() {
        this.send("CAP", "LS", "302")
        this.send("NICK", this.options.nickname)
        this.send("USER", this.options.username, "0", "*", this.options.realname)
        const capLs = await this.waitFor("CAP")
        if (!capLs) throw new Error("CAP LS timeout")

        this.serverSource = capLs.source.name

        // Add server capabilities
        for (const capability of capLs.params[2].split(" ")) {
            if (capability.startsWith("-")) {
                this.serverCapabilities.delete(capability.slice(1))
                continue
            }
            const [name, value] = capability.split("=")
            this.serverCapabilities.set(name, value ?? "")
        }

        if (this.options.auth.type === "SASL:PLAIN") await this.saslPlain(this.options.auth)

        // End CAP
        this.send("CAP", "END")
    }
    private async saslPlain(auth: Auth<"SASL:PLAIN">) {
        if (!this.serverCapabilities.has("sasl")) throw new Error("SASL not supported by server")

        // Request SASL
        this.send("CAP", "REQ", "sasl")
        const capAckOrNak = await this.waitForFilter(m => m.command === "CAP" && ["ACK", "NAK"].includes(m.params[1]))
        if (!capAckOrNak) throw new Error("CAP REQ timeout")
        if (capAckOrNak.params[1] === "NAK") throw new Error("SASL request denied by server")

        // Authenticate
        this.send("AUTHENTICATE", "PLAIN")
        const authenticateAck = await this.waitForAny(["AUTHENTICATE", Numeric.ERR_SASLFAIL])
        if (!authenticateAck) throw new Error("AUTHENTICATE timeout")
        if (authenticateAck.command !== "AUTHENTICATE") throw new Error("SASL authentication failed")

        const authString = Buffer.from(`\0${this.options.username}\0${auth.password}`).toString("base64")
        this.send("AUTHENTICATE", authString)
        const authenticateSuccess = await this.waitForAny([Numeric.RPL_SASLSUCCESS, Numeric.ERR_SASLFAIL, Numeric.ERR_SASLTOOLONG])
        if (!authenticateSuccess) throw new Error("AUTHENTICATE timeout")
        if (authenticateSuccess.command !== Numeric.RPL_SASLSUCCESS) throw new Error("SASL authentication failed")

        this.debug("[AUTHENTICATE] Success")
    }

    onMessage(message: Message) {
        switch (message.command) {
            case "PING": {
                this.send("PONG", ...message.params)
                break
            }
            case Numeric.RPL_LOGGEDIN: {
                this.isLoggedIn = true
                break
            }
            case Numeric.RPL_LOGGEDOUT: {
                this.isLoggedIn = false
                break
            }
            case Numeric.RPL_WELCOME: {
                this.emit("ready")
                this.debug("[READY]")
                break
            }

            case "JOIN": {
                const channelName = message.params[0]
                const channel = this.channels.get(channelName)
                if (!channel) return

                channel.users.add(message.source.name)

                break
            }
            case "QUIT": {
                for (const channel of this.channels.values()) {
                    channel.users.delete(message.source.name)
                }

                break
            }
            case "NICK": {
                const newNick = message.params[0]
                for (const channel of this.channels.values()) {
                    const wasRemoved = channel.users.delete(message.source.name)
                    if (wasRemoved) channel.users.add(newNick)
                }

                break
            }
        }
    }

    async join(channelName: string) {
        const channel = new Channel(this, channelName)
        await channel.join()
    }
}
