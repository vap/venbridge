const formatEscapePattern = /([\x02\x0F\x11\x16\x1D\x1E\x1F])|\x03(\d?\d?(?:,\d\d?)?)?|\x04([0-f]{6})/ig;

export const enum FormatCode {
    BOLD = "\x02",
    COLOR = "\x03",
    HEX = "\x04",
    RESET = "\x0F",
    STRIKETHROUGH = "\x1E",
    ITALIC = "\x1D",
    MONOSPACE = "\x11",
    UNDERLINE = "\x1F",
    REVERSE = "\x16",
}
export type StandaloneFormatCode = Exclude<FormatCode, FormatCode.COLOR>
export type Formatting =
    | {
        code: StandaloneFormatCode
    }
    | {
        code: FormatCode.COLOR
        foreground: number | null
        background: number | null
    }
export type FormattedText = (string | Formatting)[]

const codeToAnsiMap = [15, 0, 4, 2, 9, 1, 5, 3, 11, 10, 6, 14, 12, 13, 8, 7]

function parseCodes(text: string) {
    const segments: FormattedText = []
    const matches = text.matchAll(formatEscapePattern)

    let index = 0
    for (const match of matches) {
        const [, code, color, hex] = match
        const start = match.index!
        const end = start + match[0].length

        if (index < start) segments.push(text.slice(index, start))
        if (code) segments.push({ code: code as StandaloneFormatCode })
        if (color) {
            const [foreground, background] = color.split(",").map(n => isNaN(+n) ? null : +n)
            segments.push({ code: FormatCode.COLOR, foreground, background })
        }
        if (hex) {
            // TODO
        }

        index = end
    }
    if (index < text.length) segments.push(text.slice(index))

    return segments
}

export function stripCodes(text: string) {
    return text.replace(formatEscapePattern, "")
}

export function codesToAnsi(text: string) {
    const parsed = parseCodes(text)
    return parsed.map(segment => {
        if (typeof segment === "string") return segment
        switch (segment.code) {
            case FormatCode.BOLD: return "\x1B[1m"
            case FormatCode.COLOR: {
                if (segment.foreground == null) return "\x1B[0m"
                let ansi = ""

                let fg = codeToAnsiMap[segment.foreground % 16]
                fg = fg < 8 ? fg + 30 : fg + 82
                ansi += `\x1B[38;5;${fg}m`
                if (segment.background == null) return ansi

                let bg = codeToAnsiMap[segment.background % 16]
                bg = bg < 8 ? bg + 40 : bg + 92
                ansi += `\x1B[48;5;${bg}m`
                return ansi
            }
            case FormatCode.HEX: return "" // TODO
            case FormatCode.RESET: return "\x1B[0m"
            case FormatCode.STRIKETHROUGH: return "\x1B[9m"
            case FormatCode.ITALIC: return "\x1B[3m"
            case FormatCode.MONOSPACE: return ""
            case FormatCode.UNDERLINE: return "\x1B[4m"
            case FormatCode.REVERSE: return "\x1B[7m"
            default: return ""
        }
    }).join("")
}
