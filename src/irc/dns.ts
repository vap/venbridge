import { LookupFunction } from 'node:net'

export interface DNSQuestion {
    name: string
    type: number
}
export interface DNSAnswer {
    name: string
    type: number
    TTL: number
    data: string
}
export interface DNSRecord {
    Status: number
    TC: boolean
    RD: boolean
    RA: boolean
    AD: boolean
    CD: boolean
    Question: DNSQuestion[]
    Answer: DNSAnswer[]
}

export async function query(hostname: string) {
    const href = `https://1.1.1.1/dns-query?name=${encodeURIComponent(hostname)}&type=A`
    const data = await fetch(href, {
        headers: {
            "accept": "application/dns-json",
        },
    }).then(res => res.json()) as DNSRecord

    return data.Answer.map(answer => answer.data)
}

const lookup: LookupFunction = (hostname, options, callback) => {
    query(hostname).then(addresses => {
        callback(null, addresses[0], 4)
    }).catch(error => {
        callback(error, "", 0)
    })
}

export { lookup }
