import { Client } from './Client'

export const ircClient = new Client({
    host: process.env.IRC_HOST,
    port: +process.env.IRC_PORT,
    debug: true,

    nickname: process.env.IRC_NICKNAME,
    username: process.env.IRC_USERNAME,
    realname: process.env.IRC_REALNAME,
    auth: { type: "SASL:PLAIN", password: process.env.IRC_PASSWORD }
})

ircClient.on("ready", async () => {
    console.log("IRC client ready!")
    await ircClient.join(process.env.IRC_CHANNEL)
    console.log("Joined channel!")
})

ircClient.on("error", (error) => {
    console.error("IRC client error:", error)
})
