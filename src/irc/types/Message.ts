export interface Message {
    tags: string,
    source: { name: string, user: string, host: string, verified: boolean },
    command: string,
    params: string[],
    raw: string,
}

export function parseSource(source: string): Message["source"] | null {
    const match = /^(\S+?)(?:!(~?)(\S+?))?(?:@(\S+))?$/.exec(source)
    if (!match) return null

    const [, name, tilde, user, host] = match
    return { name, user, host, verified: !tilde }
}

export function parseMessage(message: string): Message {
    const messageMatch = /^(?:(@\S+) )?(?::(\S+) )?(\S+) (.*)$/.exec(message)
    if (!messageMatch) throw new Error("Invalid message: " + message)

    const [, tags, sourceRaw, command, rawParams] = messageMatch

    // Parse params
    let params = rawParams.split(" ")
    for (let i = 0; i < params.length; i++) {
        if (!params[i].startsWith(":")) continue
        params = params.slice(0, i).concat(params.slice(i).join(" ").slice(1))
        break
    }

    // Parse source
    const source = parseSource(sourceRaw)
    if (!source) throw new Error("Invalid source: " + sourceRaw)

    return {
        tags,
        source,
        command,
        params,
        raw: message,
    }
}
