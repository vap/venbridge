import type { Client } from '../Client'
import { Message } from './Message'
import { Numeric } from './Numerics'

export class Channel {
    topic?: string
    topicAuthor?: string
    topicCreatedAt?: Date

    users = new Set<string>()

    constructor(private client: Client, public name: string) {}

    async join() {
        if (this.client.channels.has(this.name)) return

        this.client.send("JOIN", this.name)
        // FIXME: Handle error responses
        await this.client.waitFor("JOIN", { sourcename: this.client.username })

        // FIXME: Sometimes the client has already received all of these messages before this point
        const joinFlow = await this.client.waitForFlow([
            [Numeric.RPL_TOPIC, "?"],
            [Numeric.RPL_TOPICWHOTIME, "?"],
            [Numeric.RPL_NAMREPLY, "+"],
            [Numeric.RPL_ENDOFNAMES],
        ], {
            filter: m => m.params[m.command === Numeric.RPL_NAMREPLY ? 2 : 1] === this.name,
        })
        if (!joinFlow) throw new Error("Channel join timeout")

        const [topic, topicInfo, names] = joinFlow
        this.topic = topic?.params[2]
        this.topicAuthor = topicInfo?.params[2]
        if (topicInfo?.params[3]) this.topicCreatedAt = new Date(+topicInfo.params[3] * 1000)

        names.flatMap(m => m.params[3].split(" ")).forEach(nick => {
            const prefix = /^[~&@%+]/.exec(nick)?.[0] ?? ""
            this.users.add(nick.slice(prefix.length))
        })

        this.client.channels.set(this.name, this)
    }

    say(message: string) {
        return this.client.send("PRIVMSG", this.name, message)
    }
}
