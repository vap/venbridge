import { TLSSocket, connect } from 'node:tls'
import { lookup } from '../dns'
import EventEmitter from 'eventemitter3'

export class ClientSocket extends EventEmitter<{
    error: [error: unknown]
    connect: []
    close: []
    read: [data: string]
    write: [data: string]
}> {
    private buffer = ""
    socket?: TLSSocket

    constructor(public host: string, public port: number) {
        super()
    }

    private drainBuffer() {
        const lines = this.buffer.split("\r\n")
        this.buffer = lines.pop() ?? ""

        for (const line of lines) if (line) this.emit("read", line)
    }
    write(message: string, cb?: () => void) {
        if (!this.socket) throw new Error("Socket not initialized")
        this.socket?.write(message + "\r\n", () => {
            this.emit("write", message)
            cb?.()
        })
    }
    send(command: string, ...params: string[]) {
        for (let i = 0; i < params.length; i++) {
            if (params[i].includes(" ")) {
                if (i !== params.length - 1) throw new Error("Only the last parameter can contain spaces")
                params[i] = ":" + params[i]
            }
        }
        return new Promise<void>(resolve => {
            this.write(`${command} ${params.join(" ")}`.replace(/[\r\n]/g, ""), resolve)
        })
    }

    connect() {
        const socket = this.socket = connect({
            host: this.host,
            port: this.port,
            lookup,
            rejectUnauthorized: true,
        })

        return new Promise<void>((resolve, reject) => {
            socket.setEncoding("utf-8")

            socket.on("secureConnect", () => {
                if (!socket.authorized) {
                    reject(socket.authorizationError)
                    socket.end()
                    return
                }

                resolve()
            })

            socket.on("data", (data: string) => {
                this.buffer += data
                this.drainBuffer()
            })

            // Forward events
            socket.on("error", (error) => this.emit("error", error))
            socket.on("close", () => this.emit("close"))
        })
    }

    close() {
        this.socket?.end()
    }
    destroy() {
        this.socket?.destroy()
        super.removeAllListeners()
    }
}
