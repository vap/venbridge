export type Tuple<T, N extends number> = N extends N ? number extends N ? T[] : TupleBuilder<T, N, []> : never;
type TupleBuilder<T, N extends number, R extends unknown[]> = R['length'] extends N ? R : TupleBuilder<T, N, [T, ...R]>;
