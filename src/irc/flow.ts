import type { Message } from './types/Message'
import type { Tuple } from './generics'

export type Quantifier = "*" | "+" | "?" | number
export type FlowStep = [command: string, quantifier?: Quantifier]
export type Flow = FlowStep[]
export type ReadonlyFlow = Readonly<Readonly<FlowStep>[]>

type FlowStepResult<S extends Readonly<FlowStep>> = S extends readonly [string, infer Q] ? (
    Q extends "*" ? Message[] :
    Q extends "+" ? [Message, ...Message[]] :
    Q extends "?" ? Message | null :
    Q extends number ? Tuple<Message, Q> :
    never
) : S extends readonly [string] ? Message : never

export type FlowResult<F extends ReadonlyFlow> = { [K in keyof F]: FlowStepResult<F[K]> }

const quantAsRegex = (quant?: Quantifier) => typeof quant === "number" ? `{${quant}}` : quant ?? ""

export function createFlow<F extends ReadonlyFlow>(flow: F) {
    const messages: Message[] = []
    const pattern = flow.map(([command, quant]) => `((?:${command}!)${quantAsRegex(quant)})`).join("(?:\\w+!)*?")
    const regexp = new RegExp(pattern, "d")

    function exec() {
        const trace = messages.map(m => m.command + "!").join("")
        const match = regexp.exec(trace)
        if (!match) return null

        const traceIdxToMsgIdx = (idx: number) => trace.slice(0, idx).split("!").length - 1
        const rangeToSlice = (start: number, end: number) => messages.slice(traceIdxToMsgIdx(start), traceIdxToMsgIdx(end))

        const result: (Message | Message[] | null)[] = []
        for (let i = 1; i < match.length; i++) {
            const slice = rangeToSlice(...match.indices![i])
            const quant = flow[i - 1][1]
            if (quant == null) result.push(slice[0])
            else if (quant === "?") result.push(slice[0] ?? null)
            else result.push(slice)
        }
        return result as FlowResult<F>
    }

    return [messages, exec] as const
}
