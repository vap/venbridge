import { discordClient } from './discord'
import { ircClient } from './irc'
import { createQueue } from './utils'

// TODO: Add weighted cache based on message frequency
// const CACHE_SIZE = 20
// const pfpCache = new Map<string, string>()
// const pfpFetching = new Set<string>()

// function getPfpMaybe(userId: string) {
//     const cached = pfpCache.get(userId)
//     if (cached) return cached
//     if (pfpFetching.has(userId)) return null

//     pfpFetching.add(userId)
// }

const webhookEdit = discordClient.rest.webhooks.editToken.bind(
    discordClient.rest.webhooks,
    process.env.DISCORD_WEBHOOK_ID,
    process.env.DISCORD_WEBHOOK_TOKEN,
)
const webhookExecute = discordClient.rest.webhooks.execute.bind(
    discordClient.rest.webhooks,
    process.env.DISCORD_WEBHOOK_ID,
    process.env.DISCORD_WEBHOOK_TOKEN,
)

type Event =
    | {
        type: "message"
        nickname: string
        content: string
    }
    | {
        type: "join"
        nickname: string
    }
    | {
        type: "quit"
        nickname: string
        reason?: string
    }

let lastAuthor: string | null = null
async function handleEvent(event: Event) {
    // Update webhook name/avatar if author changed
    const sender = event.type === "message" ? event.nickname : null
    if (lastAuthor !== sender) {
        await webhookEdit({
            name: sender ?? "[System]",
            avatar: null,
        })
        lastAuthor = sender
    }

    if (event.type === "message") {
        await webhookExecute({ content: event.content })
    } else if (event.type === "join") {
        await webhookExecute({ content: `**${event.nickname}** joined the channel` })
    } else if (event.type === "quit") {
        let content = `**${event.nickname}** left the channel`
        if (event.reason) content += ` (${event.reason})`
        await webhookExecute({ content })
    }
}

const pushEvent = createQueue(handleEvent)
ircClient.on("message", ({ source, command, params }) => {
    if (command === "PRIVMSG") {
        if (params[0] !== process.env.IRC_CHANNEL) return

        const message = params[1]
        const author = source.name

        pushEvent({
            type: "message",
            nickname: author,
            content: message
        })
    } else if (command === "JOIN") {
        if (params[0] !== process.env.IRC_CHANNEL) return

        pushEvent({
            type: "join",
            nickname: source.name,
        })
    } else if (command === "QUIT") {
        if (!ircClient.channels.get(process.env.IRC_CHANNEL)?.users.has(source.name)) return

        const reason = params[0]
        pushEvent({
            type: "quit",
            nickname: source.name,
            reason,
        })
    }
})
