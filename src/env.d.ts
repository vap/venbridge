declare module "supports-ansi" {
    const supportsAnsi: boolean
    export default supportsAnsi
}

declare namespace NodeJS {
    interface ProcessEnv {
        DISCORD_TOKEN: string
        DISCORD_CHANNEL: string
        DISCORD_WEBHOOK_ID: string
        DISCORD_WEBHOOK_TOKEN: string

        IRC_HOST: string
        IRC_PORT: string
        IRC_CHANNEL: string
        IRC_NICKNAME: string
        IRC_USERNAME: string
        IRC_REALNAME: string
        IRC_PASSWORD: string
    }
}
