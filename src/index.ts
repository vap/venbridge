import "dotenv/config"
import { ircClient } from "./irc"
import { discordClient } from "./discord"

import "./discord2irc"
import "./irc2discord"

ircClient.connect()
discordClient.connect()

ircClient.on("message", ({ source, command, params }) => {
    if (command === "PRIVMSG") {
        const channel = params[0]

        const args = params.at(-1)?.split(" ")
        if (!args?.[0].startsWith("!")) return
        const cmd = args.shift()!.slice(1)

        if (cmd === "ping") ircClient.send("PRIVMSG", channel, "Pong!")
    }
})
