export function createQueue<T>(handler: (item: T) => Promise<void> | void) {
    const queue: T[] = []

    async function tryNextQueue() {
        const item = queue.shift()
        if (!item) return
        await handler(item)
        tryNextQueue()
    }

    function pushQueue(item: T) {
        queue.push(item)
        tryNextQueue()
    }

    return pushQueue
}
