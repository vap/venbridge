import { Client } from "oceanic.js"

export const discordClient = new Client({
    auth: process.env.DISCORD_TOKEN,
    gateway: {
        intents: ["GUILD_MESSAGES", "MESSAGE_CONTENT"]
    }
})

discordClient.on("ready", () => {
    console.log("Discord client ready!")
})

discordClient.on("error", (error) => {
    console.error("Discord client error:", error)
})
